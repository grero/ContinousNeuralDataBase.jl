module ContinuousNeuralDataBase
using DSP
using DataProcessingHierarchyTools
const DPHT = DataProcessingHierarchyTools
import Base.zero, Base.hcat, Base.append!

struct BroadbandData <: DPHT.DPHData end
DPHT.level(::Type{BroadbandData}) = "day"

function BroadbandData()
    # check for ripple files
    datafiles = split(readchomp(`find . -name "*.ns*" -d 1`), '\n')
    if isempty(datafiles)
        datafiles = split(readchomp(`find . -name "*.pl2*" -d 1`), '\n')
        datafile = convert(String, datafiles[1])

    else
        datafile = convert(String, datafiles[1])

    end
    if isempty(datafiles)
    end
end

struct ContinuousDataArgs{T1 <: Real, T2<:Real, T3 <: DSP.FilterCoefficients} <: DPHT.DPHDataArgs
    sampling_rate::T2
    filter_coefs::T3
    filter_name::String
    filter_order::Int64
    low_freq::Float64
    high_freq::Float64
    sample_type::Type{T1}
end

function HighpassArgs(sample_type::Type{T},fs=40_000.0) where T <: Real
    lowfreq = 250.0
    highfreq = 10_000.0
    _filter = digitalfilter(Bandpass(lowfreq, highfreq, fs=fs), Butterworth(4))
    args = ContinuousDataArgs(fs, _filter, "Butterworth", 4,
                                             lowfreq,
                                             highfreq, sample_type)
    args
end

function LowpassArgs(sample_type::Type{T},fs=1000.0) where T <: Real
    lowfreq = 0.1
    highfreq = 300.0
    _filter = digitalfilter(Bandpass(lowfreq, highfreq, fs=fs), Butterworth(4))
    args = ContinuousDataArgs(fs,_filter, "Butterworth", 4,
                                             lowfreq,
                                             highfreq, sample_type)
    args
end

mutable struct ContinuousData{T1<:Real, T2<:Real, T3 <: DSP.FilterCoefficients} <: DPHT.DPHData
    data::Array{T1,1}
    channel::Int64
    args::ContinuousDataArgs{T1, T2,T3}
end


DPHT.filename(::Type{ContinuousData{T1,T2,T3}}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients = "continuousdata.mat"
DPHT.level(::Type{ContinuousData{T1,T2,T3}}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients = "channel"
DPHT.datatype(::Type{ContinuousDataArgs{T1, T2,T3}}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients = ContinuousData{T1, T2, T3}

"""
Heuristic to return the matname based on the filter frequency values
"""
function DPHT.matname(args::ContinuousDataArgs{T1, T2, T3}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients
    if args.low_freq < 100.0
        return "lowpassdata"
    else
        return "highpassdata"
    end
end

ContinuousData(sampling_rate::T2, filter_coefs::FilterCoefficients, filter_name::String, cutoff::T3) where T2 <: Real where T3 <: Real = ContinuousData(Float64[], 0, sampling_rate, filter_coefs, filter_name, cutoff)

function ContinuousData(sampling_rate::T2, low_freq::Float64, high_freq::Float64, filter_method::Function, filter_order::Int64) where T2 <: Real
    filter_coefs = digitalfilter(Continuous(cutoff;fs=sampling_rate),filter_method(filter_order))
    filter_name = convert(String, split(string(filter_method), ".")[end])
    filter_name = "$(filter_name)($(filter_order))"
    ContinuousData(sampling_rate, filter_coefs, filter_name, cutoff)
end

function Base.zero(::Type{ContinuousData{T1, T2}}) where T1 <: Real where T2 <: Real
    ContinuousData(T1[], 0, zero(T2), ZeroPoleGain([0.0], [0.0], 0.0),"", 0, 0.0, 0.0)
end

function ContinuousData(args::ContinuousDataArgs{T1,T2,T3};do_save=true, force_redo=false) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients
    # first check for old names, i.e. highpass/lowpass.mat
    _matname = DPHT.matname(args)
    fname = replace(_matname, "data" => ".mat")
    redo = !(isfile(fname) || islink(fname))
    # if not found, check of new filename
    if redo
        redo = !DPHT.computed(args)
        fname = DPHT.filename(args)
    end
    redo = force_redo || redo
    if redo == false
        hh = DPHT.load(ContinuousData{T1, T2, T3}, fname)
    else
        hh = zero(ContinuousData)
    end
    hh
end

function ContinuousData(args::ContinuousDataArgs{T1,T2,T3}, rdata::Vector{T1}, sampling_rate::Real, channel=1;do_save=true, force_redo=false) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients

    if sampling_rate > args.sampling_rate
        vdata = resample(rdata, args.sampling_rate/sampling_rate)
    else
        vdata = rdata
    end
    if args.high_freq < 1000.0
        fname = "lowpass.mat"
    else
        fname = "highpass.mat"
    end
    ldata = filtfilt(args.filter_coefs, vdata)
    X = ContinuousData(ldata, channel, args)
    if do_save
        DPHT.save(X, fname)
    end
    X
end

"""
Convenience function to load continuous data directly from a file, figuring out the data type after opening it
"""
function ContinuousData(fname::String)
    Q = MAT.matread(fname)
    if "highpassdata" in keys(Q)
        _data = Q["highpassdata"]["data"]
    else
        _data = Q["lowpassdata"]["data"]
    end
    T1 = eltype(_data["data"])
    T2 = typeof(_data["sampling_rate"])
    T3 = DSP.Filters.ZeroPoleGain{Complex{Float64},Complex{Float64},Float64}
    convert(ContinuousData{T1,T2,T3}, Q)
end

mutable struct RippleContinuousData{T1 <:Real}  <: DPHT.DPHData
    data::Vector{T1}
    channel::Int64
    sampling_rate::Float64
    low_freq::Float64
    high_freq::Float64
end

DPHT.filename(::Type{RippleContinuousData}) = "rplhighpass.mat"
DPHT.level(::Type{RippleContinuousData}) = "channel"

function RippleContinuousData()
    fname = filename(RippleContinuousData)
    HDF5.h5open(fname,"r") do ff
        datapath = "rh/data/analogData"
        if ismmappable(ff[datapath])
            data = readmmap(ff[datapath])
        else
            data = read(ff, datapath)
        end
        b1,b2 = splitdir(pwd())
        channel = parse(Int64, filter(isdigit,b2))
        sampling_rate = read(ff, "rh/data/analogInfo/SampleRate")[1]
        low_freq = read(ff["rh/data/analogInfo"]["HighFreqCorner"])[1]/1000
        high_freq = read(ff["rh/data/analogInfo"]["LowFreqCorner"])[1]/1000
        RippleContinuousData(data[ :, 1],channel, sampling_rate, low_freq, high_freq)
    end
end

function Base.convert(::Type{Dict{String, Any}}, X::ContinuousData{T1,T2,T3}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients
    mat_dict = Dict{String,Any}()
    for k in fieldnames(typeof(X))
        if k == :args
            continue
        end
        mat_dict[string(k)] = getfield(X, k)
    end
    args_dict = convert(Dict{String,Any}, X.args)
    merge!(mat_dict, args_dict)
    return convert(Dict{String, Any}, Dict(DPHT.matname(X.args) => Dict("data" => mat_dict)))
end

function Base.convert(::Type{Dict{String, Any}}, X::ContinuousDataArgs{T1, T2,T3}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients
    mat_dict = Dict{String, Any}()
    for k in fieldnames(typeof(X))
        if k == :filter_coefs
            bb = getfield(X,k)
            mat_dict[string(k)] = Dict("z" => bb.z, "p" => bb.p, "k" => bb.k)
        elseif k == :sample_type
            continue
        else
            mat_dict[string(k)] = getfield(X, k)
        end
    end
    return mat_dict
end

function Base.convert(::Type{ContinuousData{T1,T2,T3}}, Q::Dict{String,Any}) where T1 <: Real where T2 <: Real where T3 <: DSP.FilterCoefficients
    if "highpassdata" in keys(Q)
        _data = Q["highpassdata"]["data"]
    else
        _data = Q["lowpassdata"]["data"]
    end
    tt = eltype(_data["data"])
    fn = _data["filter_name"]
    fo = _data["filter_order"]
    bb = _data["filter_coefs"]
    ff = ZeroPoleGain(bb["z"], bb["p"], bb["k"])
    args = ContinuousDataArgs(_data["sampling_rate"],
                              ff, fn,fo, _data["low_freq"], _data["high_freq"], tt)
    X = ContinuousData(_data["data"], _data["channel"], args)
end
end#module

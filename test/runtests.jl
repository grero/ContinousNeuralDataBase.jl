using ContinuousNeuralDataBase
using ContinuousNeuralDataBase: LowpassArgs, HighpassArgs, ContinuousData
using DataProcessingHierarchyTools
const DPHT = DataProcessingHierarchyTools
using MAT
using Test
using Random

tdir = tempdir()
cd(tdir) do
    @testset "Basic" begin
        X = fill(0.0, 30_000)
        RNG = MersenneTwister(UInt32(1234))
        X[1] = randn(RNG)
        #brownian motion
        for i in 2:length(X)
            X[i] = X[i] + randn(RNG)
        end
        largs = LowpassArgs(Float64)
        @test largs.sampling_rate == 1000.0
        cdata = ContinuousData(largs, X, 30_000)
        @test length(cdata.data) == 1000
        @test isfile("lowpass.mat")
        Q = MAT.matread("lowpass.mat")
        cdata2 = convert(typeof(cdata), Q)
        @test cdata2.data ≈ cdata.data
        #TODO: test that the data actually makes sense
    end
end
